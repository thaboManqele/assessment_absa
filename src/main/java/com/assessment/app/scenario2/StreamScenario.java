package com.assessment.app.scenario2;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamScenario {
    public static void main(String[] args) {
        List<String> statuses = Arrays.asList("Received", "Pending", "","Awaiting Maturity", "Completed","");
        List<String> filterList = statuses
                                 .stream()
                                 .filter(s->!s.isEmpty())
                                 .collect(Collectors.toList());

        System.out.println(filterList.toString());
    }
}
