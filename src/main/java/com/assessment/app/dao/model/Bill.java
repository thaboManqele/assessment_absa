package com.assessment.app.dao.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name ="bill")
public class Bill implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name="transactionReference")
    private String transactionReference;

    @Column(name="clientSwiftAddress")
    private String clientSwiftAddress;

    @Column(name="messageStatus")
    private String messageStatus;

    @Column(name="currency")
    private String currency;

    @Column(name="amount")
    private Long amount;

    @Column(name="dateTimeCreated")
    private Date dateTimeCreated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getClientSwiftAddress() {
        return clientSwiftAddress;
    }

    public void setClientSwiftAddress(String clientSwiftAddress) {
        this.clientSwiftAddress = clientSwiftAddress;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Date getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setDateTimeCreated(Date dateTimeCreated) {
        this.dateTimeCreated = dateTimeCreated;
    }
}
