package com.assessment.app.dao.repository;

import com.assessment.app.dao.model.Bill;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
public interface BillRepository extends JpaRepository<Bill, Long> {
    List<Bill> findAll();
    Bill findBillById(Long id);
}
