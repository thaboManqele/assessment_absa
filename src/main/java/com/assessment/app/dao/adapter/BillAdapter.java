package com.assessment.app.dao.adapter;

import com.assessment.app.dao.model.Bill;
import com.assessment.app.dao.model.BillDTO;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;

@Component
public class BillAdapter {
    private final ModelMapper modelMapper;

    @Autowired
    public BillAdapter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<BillDTO> adapt(List<Bill> bills){
        Type billDTOs = new TypeToken<List<BillDTO>>() {}.getType();
        return this.modelMapper.map(bills,billDTOs);
    }
}
