package com.assessment.app.api;

import com.assessment.app.dao.model.Bill;
import com.assessment.app.dao.model.BillDTO;
import com.assessment.app.dao.model.History;

import java.util.List;

public interface BillService {
   Bill getBillById(Long id);
   List<BillDTO> findAll();
   History saveHistory(History history);
}
