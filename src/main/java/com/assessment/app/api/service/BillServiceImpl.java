package com.assessment.app.api.service;

import com.assessment.app.api.BillService;
import com.assessment.app.dao.adapter.BillAdapter;
import com.assessment.app.dao.model.Bill;
import com.assessment.app.dao.model.BillDTO;
import com.assessment.app.dao.model.History;
import com.assessment.app.dao.repository.BillRepository;
import com.assessment.app.dao.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class BillServiceImpl implements BillService {
    private final BillRepository billRepository;
    private final HistoryRepository historyRepository;
    private final BillAdapter billAdapter;

    @Autowired
    public BillServiceImpl(BillRepository billRepository, HistoryRepository historyRepository, BillAdapter billAdapter) {
        this.billRepository = billRepository;
        this.historyRepository = historyRepository;
        this.billAdapter = billAdapter;
    }

    @Override
    public Bill getBillById(Long id) {
        return this.billRepository.findBillById(id);
    }

    @Override
    public List<BillDTO> findAll() {
        List<BillDTO> unOrderd = this.billAdapter.adapt(this.billRepository.findAll());
        List<BillDTO> swiftOrderList = groupByClientSwiftAddress(unOrderd);
        List<BillDTO> currencyOrderList = groupByCurrency(swiftOrderList);

        return updateCurrencyToSubService(currencyOrderList);
    }

    @Override
    public History saveHistory(History history) {
       return historyRepository.saveAndFlush(history);
    }

    private List<BillDTO> groupByClientSwiftAddress(List<BillDTO> billDTOS){
        Map<String, List<BillDTO>> swiftIdGroup = billDTOS
                .stream()
                .collect(Collectors.groupingBy(BillDTO::getClientSwiftAddress));

        return swiftIdGroup.values().stream().flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private List<BillDTO> groupByCurrency(List<BillDTO> billDTOS){

        Map<String, List<BillDTO>> currencyGroup = billDTOS
                .stream()
                .collect(Collectors.groupingBy(BillDTO::getCurrency));

        return currencyGroup.values().stream().flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private List<BillDTO> updateCurrencyToSubService(List<BillDTO> billDTOS){
        return billDTOS.stream().peek(billDTO -> {
            String currency = billDTO.getCurrency();
            billDTO.setCurrency((currency.equals("ZAR"))?"ZAROUT":"CCYOUT");
        }).collect(Collectors.toList());
    }

}
