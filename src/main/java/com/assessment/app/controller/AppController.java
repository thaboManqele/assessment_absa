package com.assessment.app.controller;

import com.assessment.app.api.BillService;
import com.assessment.app.dao.model.Bill;
import com.assessment.app.dao.model.BillDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AppController {
    private final BillService billService;

    @Autowired
    public AppController(BillService billServiceImpl) {
        this.billService = billServiceImpl;
    }

    @GetMapping("/get/bill/{id}")
    public Bill getBillById(@PathVariable Long id){
        return this.billService.getBillById(id);
    }

    @GetMapping("/get/all/bill/")
    public List<BillDTO> getAllBills(@PathVariable Long id){
        return this.billService.findAll();
    }
}
