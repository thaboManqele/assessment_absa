package com.assessment.app.batchConfig;

import com.assessment.app.dao.model.BillDTO;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

@Configuration
@EnableScheduling
@EnableBatchProcessing
public class BillReportConfig {
    private static final int CHUNK = 100;
    private static final String OUTPUT_FILE ="output.txt" ;
    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final JobLauncher jobLauncher;

    @Autowired
    private Step billExportStep; //resolves circular dependency caused by injecting step in constructor

    @Autowired
    public BillReportConfig(JobBuilderFactory jobBuilders, StepBuilderFactory stepBuilders, JobLauncher jobLauncher) {
        this.jobBuilderFactory = jobBuilders;
        this.stepBuilderFactory = stepBuilders;
        this.jobLauncher = jobLauncher;
    }

    @Scheduled(cron = "0 0 0 1/7 * ?")
    public void run() throws Exception {
        if(isWorkDay()) {
            JobExecution execution = jobLauncher.run(
                    exportBillReportToFile(billExportStep),
                    new JobParametersBuilder().addString("uuid", this.uuidGenerator()).toJobParameters()
            );
        }
    }

    @Bean
    public Job exportBillReportToFile(Step billExportStep) {
        return jobBuilderFactory.get("exportBillReportToFile")
                .start(billExportStep)
                .build();
    }

    @Bean
    public Step billExportStep(ItemReader<BillDTO> reader, ItemWriter<BillDTO> writer) {
        return stepBuilderFactory.get("billExportStep")
                .<BillDTO, BillDTO>chunk(CHUNK)
                .reader(reader)
                .writer(writer)
                .build();
    }


    private String uuidGenerator() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    private boolean isWorkDay(){
        Calendar myDate = Calendar.getInstance();
        int dow = myDate.get (Calendar.DAY_OF_WEEK);
        return ((dow >= Calendar.MONDAY) && (dow <= Calendar.FRIDAY));
    }


}
