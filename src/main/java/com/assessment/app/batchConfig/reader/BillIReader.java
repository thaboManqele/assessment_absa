package com.assessment.app.batchConfig.reader;

import com.assessment.app.api.BillService;
import com.assessment.app.dao.model.BillDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.support.IteratorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class BillIReader implements ItemReader<BillDTO> {
    private static final Logger logger = LogManager.getLogger(BillIReader.class);
    private final List<BillDTO> bills;
    private ItemReader<BillDTO> reader;

    @Autowired
    public BillIReader(BillService billService) {
        bills = billService.findAll();
    }

    @Override
    public BillDTO read() throws Exception {
        if (reader == null) {
            reader = new IteratorItemReader<>(this.bills);
        }
        return reader.read();
    }

}
