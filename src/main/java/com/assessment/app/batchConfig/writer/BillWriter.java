package com.assessment.app.batchConfig.writer;

import com.assessment.app.dao.model.BillDTO;
import com.assessment.app.dao.model.History;
import com.assessment.app.dao.repository.HistoryRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.*;
import java.util.List;

@Component
public class BillWriter implements Closeable, ItemWriter<BillDTO> {
private static final Logger logger = LogManager.getLogger(BillWriter.class);
    private static final String OUTPUT_FILE ="output.txt" ;
    private final PrintWriter writer;
    private final HistoryRepository history;

    public BillWriter(HistoryRepository history) {
        this.history = history;
        logger.info("Started Writer");
        OutputStream out;
        try {
            out = new FileOutputStream("output.txt");
        } catch (FileNotFoundException e) {
            out = System.out;
        }
        this.writer = new PrintWriter(out);
    }

    @Override
    public void write(List<? extends BillDTO> bills) throws Exception {
        long countZar = bills.stream().filter(i -> ((BillDTO) i).getCurrency().equals("ZAR")).count();
        long countCcy = bills.stream().filter(i -> !((BillDTO) i).getCurrency().equals("ZAR")).count();

        for (BillDTO bill : bills) {
            long count = 0;
            logger.info("Writing to file");
            logger.info(bill.getClientSwiftAddress());
            count = (bill.getCurrency().equals("ZAR"))?countZar:countCcy;
            history.saveAndFlush(new History(bill.toString()+count));
            writer.println(bill.toString()+count);
        }
        writer.flush();
        writer.close();
    }



    @PreDestroy
    @Override
    public void close() throws IOException {
        writer.flush();
        writer.close();
    }

}
