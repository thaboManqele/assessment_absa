-- Table: public."Bill"

-- DROP TABLE public."Bill";

CREATE TABLE public."Bill"
(
    id integer NOT NULL DEFAULT nextval('bill_id'::regclass),
    "transactionReference" character varying(16)[] COLLATE pg_catalog."default",
    "clientSwiftAddress" character varying(12)[] COLLATE pg_catalog."default",
    "messageStatus" character varying(9)[] COLLATE pg_catalog."default",
    "currency" character varying(3)[] COLLATE pg_catalog."default",
    "amount" numeric(11,2)[],
    "dateTimeCreated" timestamp(6) without time zone,
    CONSTRAINT pk_id PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public."Bill"
    OWNER to postgres;

GRANT ALL ON TABLE public."Bill" TO postgres;

/*INSERT INTO Bill (id, transactionReference, clientSwiftAddress, messageStatus ,currency, amount, dateTimeCreated)
VALUES (null, 'registration', 'HSBCXXXYYYY', 'Completed' ,'ZAROUT', 10000000.00, '11-12-2019');*/
