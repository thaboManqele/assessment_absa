# Scenario 1
For a problem as small as this there was no need to use spring batch but I thought 
it would be a good challenge to demonstrate the solution, assuming the problem space was bigger and batch would be an appropriate solution

## Uses Mysql db but can be changed to any db of course
Found it simpler to go with mysql. The Batch ddl file and create are available

##NB: The file is dropped on root.(There's no upload app)


# Scenario 2
Use of parallel streams is demonstrated in in the package scenario2. Please run the class.
