CREATE TABLE `bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionReference` varchar(16) DEFAULT NULL,
  `clientSwiftAddress` varchar(12) DEFAULT NULL,
  `messageStatus` varchar(9) DEFAULT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `amount` decimal(11,2) DEFAULT NULL,
  `dateTimeCreated` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `history` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

